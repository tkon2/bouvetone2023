import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm'
import SpeakerNotes from 'reveal.js/plugin/notes/notes.esm'

let deck = new Reveal({
   plugins: [
      Markdown,
      SpeakerNotes,
   ],
   progress: false,
})
deck.initialize();
